package by.sl.devstudio.com.shoplistv4.model.product;

import javax.inject.Inject;

import by.sl.devstudio.com.shoplistv4.ApplicationShopList;
import by.sl.devstudio.com.shoplistv4.model.IModelChangeListener;
import by.sl.devstudio.com.shoplistv4.model.group.DBGroup;
import by.sl.devstudio.com.shoplistv4.model.group.ModelGroup;
import io.realm.Realm;
import io.realm.RealmResults;

public class ModelListProduct implements IModelListProduct {
    @Inject
    Realm realm;

    IModelChangeListener modelChangeListener;

    public ModelListProduct() {
        ApplicationShopList.getComponent().inject(this);
    }

    @Override
    public long create(String name, long idGroup) {

        realm.beginTransaction();

        Number num = realm.where(DBListProduct.class).max("id");
        int nextID;
        if (num == null) {
            nextID = 1;
        } else {
            nextID = num.intValue() + 1;
        }

        DBListProduct obj = realm.createObject(DBListProduct.class, nextID);
        obj.setName(name);

        ModelGroup modelGroup = new ModelGroup();
        RealmResults<DBGroup> realmResults = modelGroup.findById(idGroup);
        if (realmResults.size() > 0) {
            obj.setDbGroup(modelGroup.findById(idGroup).get(0));
        } else {
            realm.cancelTransaction();
            return -1;
        }

        realm.commitTransaction();

        modelChangeListener.onChange();

        return nextID;
    }

    @Override
    public boolean rename(long id, String newName) {
        RealmResults<DBListProduct> dbListProducts = realm.where(DBListProduct.class).equalTo("id", id).findAll();
        if (dbListProducts.size() > 0) {
            realm.beginTransaction();
            dbListProducts.get(0).setName(newName);
            realm.commitTransaction();
            modelChangeListener.onChange();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean delete(long id) {
        try {
            realm.beginTransaction();
            findById(id).deleteAllFromRealm();
            realm.commitTransaction();
            modelChangeListener.onChange();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public RealmResults<DBListProduct> findAllByGroupId(long idGroup) {
        return realm.where(DBListProduct.class).equalTo("dbGroup.id", idGroup).findAll();
    }

    @Override
    public RealmResults<DBListProduct> findById(long id) {
        return realm.where(DBListProduct.class).equalTo("id", id).findAll();
    }

    @Override
    public boolean setSelected(long id) {

        try {
            RealmResults<DBListProduct> dbGroupRealmResults = findById(id);
            realm.beginTransaction();
            for (DBListProduct element : dbGroupRealmResults) {
                element.setSelected(!element.isSelected());
            }
            realm.commitTransaction();
            modelChangeListener.onChange();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public long getSelectedId(long idGroup) {
        RealmResults<DBListProduct> realmResults = realm.where(DBListProduct.class).equalTo("dbGroup.id", idGroup).and().equalTo("selected",true).findAll();
        if(realmResults.size()==1){
            return realmResults.get(0).getId();
        }

        if(realmResults.size()>1){
            return -2;
        }

        return -1;
    }

    @Override
    public void setListener(final IModelChangeListener modelChangeListener) {
        this.modelChangeListener = modelChangeListener;
    }
}
