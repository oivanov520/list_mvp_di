package by.sl.devstudio.com.shoplistv4.presenter;

import android.support.v7.app.AppCompatActivity;

import by.sl.devstudio.com.shoplistv4.model.group.DBGroup;
import by.sl.devstudio.com.shoplistv4.model.product.DBListProduct;
import io.realm.RealmResults;

public interface IPresenterMainActivity {

    void onCreateView(AppCompatActivity activity);

    void onClickAddGroup(String name);

    void onClickItemGroup(long id);

    void onClickDeleteGroup(long id);

    void onClickRenameGroup(long id, String newName);

    void setListener(CallBackMainActivity callBackMainActivity);

    RealmResults<DBGroup> getGroup();

    RealmResults<DBListProduct> getListProduct();

    String getNameGroup(long id);

    void onClickAddProduct(String name);

    void onClickItemProduct(long id);

}
