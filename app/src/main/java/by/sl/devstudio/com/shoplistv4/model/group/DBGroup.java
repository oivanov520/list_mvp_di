package by.sl.devstudio.com.shoplistv4.model.group;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DBGroup extends RealmObject {
    @PrimaryKey
    long id;
    String name;
    boolean selected;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
