package by.sl.devstudio.com.shoplistv4.model.group;

import by.sl.devstudio.com.shoplistv4.model.IModelChangeListener;
import io.realm.RealmResults;

public interface IModelGroup {
    long create(String name);
    boolean rename(long id, String newName);
    boolean delete(long id);
    RealmResults<DBGroup> findAll();
    RealmResults<DBGroup> findById(long id);
    RealmResults<DBGroup> findByName(String name);
    boolean setSelected(long id);
    long getSelectedId();
    void setListener(IModelChangeListener modelChangeListener);

}
