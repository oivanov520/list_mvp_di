package by.sl.devstudio.com.shoplistv4;

import javax.inject.Singleton;

import by.sl.devstudio.com.shoplistv4.model.group.ModelGroup;
import by.sl.devstudio.com.shoplistv4.model.product.ModelListProduct;
import by.sl.devstudio.com.shoplistv4.presenter.PresenterMainActivity;
import by.sl.devstudio.com.shoplistv4.ui.GroupActivity;
import by.sl.devstudio.com.shoplistv4.ui.MainActivity;
import dagger.Component;

@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent {
    void inject(MainActivity param);
    void inject(GroupActivity param);
    void inject(ModelGroup modelGroup);
    void inject(ModelListProduct modelListProduct);
    void inject(PresenterMainActivity presenterMainActivity);

}