package by.sl.devstudio.com.shoplistv4.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import by.sl.devstudio.com.shoplistv4.ApplicationShopList;
import by.sl.devstudio.com.shoplistv4.R;
import by.sl.devstudio.com.shoplistv4.presenter.IPresenterMainActivity;

public class GroupActivity extends AppCompatActivity {

    @Inject
    IPresenterMainActivity presenterMainActivity;

    EditText etNameGroup;
    long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        ApplicationShopList.getComponent().inject(this);

        init();
    }

    private void init() {
        etNameGroup = ((EditText) findViewById(R.id.etNameGroup));
        id = getIntent().getLongExtra("id", -1);
        if (id > -1) {
            etNameGroup.setText(presenterMainActivity.getNameGroup(id));
        }

    }

    public void onButtonOk(View view) {
        if (id < 0) {
            if(!etNameGroup.getText().toString().isEmpty()) {
                presenterMainActivity.onClickAddGroup(etNameGroup.getText().toString());
            }else {
                Toast.makeText(this, getString(R.string.msg_enter_name_group), Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            presenterMainActivity.onClickRenameGroup(id, etNameGroup.getText().toString());
        }
        finish();

    }

    public void onButtonCancel(View view) {
        finish();

    }

    public void onDelete(View view) {
        presenterMainActivity.onClickDeleteGroup(id);
        finish();

    }
}
