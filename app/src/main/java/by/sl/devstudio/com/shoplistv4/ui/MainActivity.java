package by.sl.devstudio.com.shoplistv4.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.Date;

import javax.inject.Inject;

import by.sl.devstudio.com.shoplistv4.ApplicationShopList;
import by.sl.devstudio.com.shoplistv4.R;
import by.sl.devstudio.com.shoplistv4.presenter.CallBackMainActivity;
import by.sl.devstudio.com.shoplistv4.presenter.IPresenterMainActivity;
import by.sl.devstudio.com.shoplistv4.ui.adapter.AdapterGroup;
import by.sl.devstudio.com.shoplistv4.ui.adapter.AdapterListShop;
import by.sl.devstudio.com.shoplistv4.ui.adapter.CallBackAdapter;

public class MainActivity extends AppCompatActivity {

    final String TAG = "TESTER";
    final int OPEN_CREATE_NEW_GROUP_DIALOG = 1001;
    final int OPEN_CHANGE_GROUP_DIALOG = 1002;

    @Inject
    IPresenterMainActivity presenterMainActivity;

    RecyclerView rwListGroup;
    AdapterGroup adapterGroup;

    RecyclerView rwListShop;
    AdapterListShop adapterListShop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ApplicationShopList.getComponent().inject(this);

        rwListGroup = findViewById(R.id.rwListGroup);
        rwListShop  = findViewById(R.id.rwListShop);

        presenterMainActivity.onCreateView(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshGroup();
        refreshListShop();
        setListener();
    }

    private void setListener() {
        presenterMainActivity.setListener(new CallBackMainActivity() {
            @Override
            public void onChangedGroup() {
                notifyAdapterGroup();
            }

            @Override
            public void onChangeListProduct() {
                notifyAdapterListProduct();
            }
        });
    }

    public void addGroup(View view) {
        Intent intent = new Intent(this, GroupActivity.class);
        startActivityForResult(intent, OPEN_CREATE_NEW_GROUP_DIALOG);
    }

    private void refreshGroup() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rwListGroup.setLayoutManager(linearLayoutManager);
        adapterGroup = new AdapterGroup(MainActivity.this, new CallBackAdapter() {
            @Override
            public void onClickItem(long id) {
                presenterMainActivity.onClickItemGroup(id);
            }

            @Override
            public void onLongClickItem(long id) {
                Intent intent = new Intent(MainActivity.this, GroupActivity.class);
                intent.putExtra("id",id);
                startActivityForResult(intent, OPEN_CHANGE_GROUP_DIALOG);
            }
        });
        adapterGroup.setDbGroupRealmResults(presenterMainActivity.getGroup());
        rwListGroup.setAdapter(adapterGroup);
    }

    private void refreshListShop() {
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        rwListShop.setLayoutManager(linearLayoutManager);
        adapterListShop = new AdapterListShop(MainActivity.this, new CallBackAdapter() {
            @Override
            public void onClickItem(long id) {
                presenterMainActivity.onClickItemProduct(id);
            }

            @Override
            public void onLongClickItem(long id) {
            }
        });

        adapterListShop.setDbGroupRealmResults(presenterMainActivity.getListProduct());
        rwListShop.setAdapter(adapterListShop);
    }

    private void notifyAdapterGroup() {

        runOnUiThread(() -> {
            adapterGroup.notifyDataSetChanged();
            refreshListShop();
        });

    }

    private void notifyAdapterListProduct() {
        runOnUiThread(() -> adapterListShop.notifyDataSetChanged());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setListener();
    }

    public void addProduct(View view) {
        presenterMainActivity.onClickAddProduct("TEST "+new Date().getTime());
    }
}
