package by.sl.devstudio.com.shoplistv4.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import by.sl.devstudio.com.shoplistv4.R;
import by.sl.devstudio.com.shoplistv4.model.group.DBGroup;
import io.realm.RealmResults;

public class AdapterGroup extends RecyclerView.Adapter<AdapterGroup.MessageViewHolder> {

    RealmResults<DBGroup> dbGroupRealmResults = null;
    Context context;
    CallBackAdapter callBackAdapter;
    MessageViewHolder currentHolder;


    public AdapterGroup(Context context, CallBackAdapter callBackAdapter) {
        this.context = context;
        this.callBackAdapter = callBackAdapter;
    }

    public RealmResults<DBGroup> getDbGroupRealmResults() {
        return dbGroupRealmResults;
    }

    public void setDbGroupRealmResults(RealmResults<DBGroup> dbGroupRealmResults) {
        this.dbGroupRealmResults = dbGroupRealmResults;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_group, viewGroup, false);
        MessageViewHolder messageViewHolder = new MessageViewHolder(v);
        return messageViewHolder;
    }

    @Override
    public void onViewAttachedToWindow(@NonNull MessageViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        currentHolder = holder;
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull MessageViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public void onBindViewHolder(final MessageViewHolder viewHolder, final int i) {
        viewHolder.tvGroupName.setText("" + dbGroupRealmResults.get(i).getName());
        viewHolder.tvGroupName.setTextColor(dbGroupRealmResults.get(i).isSelected() ? Color.BLACK : Color.GRAY);
        viewHolder.tvGroupName.setBackgroundColor(dbGroupRealmResults.get(i).isSelected() ? Color.WHITE : Color.BLACK);

        viewHolder.clItem.setOnClickListener(view -> callBackAdapter.onClickItem(dbGroupRealmResults.get(i).getId()));

        viewHolder.clItem.setOnLongClickListener(view -> {
            callBackAdapter.onLongClickItem(dbGroupRealmResults.get(i).getId());
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return dbGroupRealmResults.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView tvGroupName;
        ConstraintLayout clItem;
        View mView;

        public MessageViewHolder(View itemView) {
            super(itemView);
            tvGroupName = (TextView) itemView.findViewById(R.id.tvGroupName);
            clItem = (ConstraintLayout) itemView.findViewById(R.id.clItem);
            mView = itemView;
        }
    }
}
