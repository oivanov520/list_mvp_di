package by.sl.devstudio.com.shoplistv4.presenter;

public interface CallBackMainActivity {
    void onChangedGroup();

    void onChangeListProduct();
}
