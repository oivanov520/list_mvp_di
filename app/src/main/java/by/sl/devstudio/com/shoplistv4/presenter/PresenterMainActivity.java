package by.sl.devstudio.com.shoplistv4.presenter;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import javax.inject.Inject;

import by.sl.devstudio.com.shoplistv4.ApplicationShopList;
import by.sl.devstudio.com.shoplistv4.model.IModelChangeListener;
import by.sl.devstudio.com.shoplistv4.model.group.DBGroup;
import by.sl.devstudio.com.shoplistv4.model.group.ModelGroup;
import by.sl.devstudio.com.shoplistv4.model.product.DBListProduct;
import by.sl.devstudio.com.shoplistv4.model.product.ModelListProduct;
import io.realm.RealmResults;

public class PresenterMainActivity implements IPresenterMainActivity {
    @Inject
    ModelGroup modelGroup;

    @Inject
    ModelListProduct modelListProduct;


    @Override
    public void onCreateView(AppCompatActivity activity) {
        ApplicationShopList.getComponent().inject(this);
    }

    @Override
    public void onClickAddGroup(String name) {
        modelGroup.create(name);
    }

    @Override
    public void onClickItemGroup(long id) {
        modelGroup.setSelected(id);
    }

    @Override
    public void onClickDeleteGroup(long id) {
        modelGroup.delete(id);
    }

    @Override
    public void onClickRenameGroup(long id, String newName) {
        modelGroup.rename(id, newName);
    }

    @Override
    public void setListener(CallBackMainActivity callBackMainActivity) {
        modelGroup.setListener(() -> callBackMainActivity.onChangedGroup());
        modelListProduct.setListener(() -> callBackMainActivity.onChangeListProduct());

    }

    @Override
    public RealmResults<DBGroup> getGroup() {
        return modelGroup.findAll();
    }

    @Override
    public RealmResults<DBListProduct> getListProduct() {
        return modelListProduct.findAllByGroupId(modelGroup.getSelectedId());
    }

    @Override
    public String getNameGroup(long id) {
        try {
            return modelGroup.findById(id).get(0).getName();
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public void onClickAddProduct(String name) {
        modelListProduct.create(name, modelGroup.getSelectedId());
    }

    @Override
    public void onClickItemProduct(long id) {
        modelListProduct.setSelected(id);
    }
}
