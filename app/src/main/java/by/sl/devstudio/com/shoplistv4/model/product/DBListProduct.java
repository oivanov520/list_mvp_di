package by.sl.devstudio.com.shoplistv4.model.product;

import by.sl.devstudio.com.shoplistv4.model.group.DBGroup;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DBListProduct extends RealmObject {
    @PrimaryKey
    long id;
    String name;
    boolean selected;
    DBGroup dbGroup;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public DBGroup getDbGroup() {
        return dbGroup;
    }

    public void setDbGroup(DBGroup dbGroup) {
        this.dbGroup = dbGroup;
    }
}
