package by.sl.devstudio.com.shoplistv4.model.product;

import by.sl.devstudio.com.shoplistv4.model.IModelChangeListener;
import io.realm.RealmResults;

public interface IModelListProduct {
    long create(String name, long idGroup);
    boolean rename(long id, String newName);
    boolean delete(long id);
    RealmResults<DBListProduct> findAllByGroupId(long idGroup);
    RealmResults<DBListProduct> findById(long id);
    boolean setSelected(long id);
    long getSelectedId(long idGroup);
    void setListener(IModelChangeListener modelChangeListener);
}
