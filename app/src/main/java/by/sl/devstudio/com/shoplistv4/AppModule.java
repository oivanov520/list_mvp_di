package by.sl.devstudio.com.shoplistv4;

import android.content.Context;
import android.view.Display;

import java.lang.ref.WeakReference;

import javax.inject.Singleton;

import by.sl.devstudio.com.shoplistv4.model.group.ModelGroup;
import by.sl.devstudio.com.shoplistv4.model.product.ModelListProduct;
import by.sl.devstudio.com.shoplistv4.presenter.IPresenterMainActivity;
import by.sl.devstudio.com.shoplistv4.presenter.PresenterMainActivity;
import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;

@Module
class AppModule {

    private WeakReference<Context> refContext;
    private Realm realm;

    AppModule(Context context) {
        this.refContext = new WeakReference<>(context);

        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        this.realm = Realm.getDefaultInstance();

        /*
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
        */
    }

    //DATA BASE
    @Provides
    @Singleton
    Realm getRealm() {
        return realm;
    }

    //MODEL
    @Provides
    @Singleton
    ModelGroup getModelGroup() {
        return new ModelGroup();
    }

    @Provides
    @Singleton
    ModelListProduct getModelListProduct() {
        return new ModelListProduct();
    }

    //UI
    @Provides
    @Singleton
    IPresenterMainActivity getPresenterMainActivity() {
        return new PresenterMainActivity();
    }

}
