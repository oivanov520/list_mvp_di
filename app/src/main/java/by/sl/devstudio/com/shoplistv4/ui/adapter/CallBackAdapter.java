package by.sl.devstudio.com.shoplistv4.ui.adapter;

public interface CallBackAdapter {
    void onClickItem(long id);
    void onLongClickItem(long id);
}
