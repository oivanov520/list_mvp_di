package by.sl.devstudio.com.shoplistv4.model.group;

import javax.inject.Inject;

import by.sl.devstudio.com.shoplistv4.ApplicationShopList;
import by.sl.devstudio.com.shoplistv4.model.IModelChangeListener;
import by.sl.devstudio.com.shoplistv4.model.product.ModelListProduct;
import io.realm.Realm;
import io.realm.RealmResults;

public class ModelGroup implements IModelGroup {

    @Inject
    Realm realm;

    IModelChangeListener modelChangeListener;

    public ModelGroup() {
        ApplicationShopList.getComponent().inject(this);
    }

    @Override
    public long create(String name) {
        realm.beginTransaction();

        Number num = realm.where(DBGroup.class).max("id");
        int nextID;
        if (num == null) {
            nextID = 1;
        } else {
            nextID = num.intValue() + 1;
        }

        DBGroup obj = realm.createObject(DBGroup.class, nextID);
        obj.setName(name);
        obj.setSelected(true);

        realm.commitTransaction();

        setSelected(nextID);

        return nextID;
    }

    @Override
    public boolean rename(long id, String newName) {
        RealmResults<DBGroup> dbGroupRealmResults = realm.where(DBGroup.class).equalTo("id", id).findAll();
        if (dbGroupRealmResults.size() > 0) {
            realm.beginTransaction();
            dbGroupRealmResults.get(0).setName(newName);
            realm.commitTransaction();

            modelChangeListener.onChange();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean delete(long id) {
        try {
            realm.beginTransaction();
            findById(id).deleteAllFromRealm();
            realm.commitTransaction();

            realm.beginTransaction();

            new ModelListProduct().findAllByGroupId(id).deleteAllFromRealm();

            realm.commitTransaction();

            modelChangeListener.onChange();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public RealmResults<DBGroup> findAll() {
        return realm.where(DBGroup.class).findAll();
    }

    @Override
    public RealmResults<DBGroup> findById(long id) {
        return realm.where(DBGroup.class).equalTo("id", id).findAll();
    }

    @Override
    public RealmResults<DBGroup> findByName(String name) {
        return realm.where(DBGroup.class).equalTo("name", name).findAll();
    }

    @Override
    public boolean setSelected(long id) {

        try {
            RealmResults<DBGroup> dbGroupRealmResults = findAll();
            realm.beginTransaction();
            for (DBGroup element : dbGroupRealmResults) {
                if (element.getId() == id) {
                    element.setSelected(true);
                } else {
                    element.setSelected(false);
                }
            }
            realm.commitTransaction();
            modelChangeListener.onChange();
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public long getSelectedId() {
        RealmResults<DBGroup> realmResults = realm.where(DBGroup.class).equalTo("selected", true).findAll();

        if (realmResults.size() == 1) {
            return realmResults.get(0).getId();
        }

        if (realmResults.size() > 1) {
            return -2;
        }

        return -1;
    }

    @Override
    public void setListener(IModelChangeListener modelChangeListener) {
        this.modelChangeListener = modelChangeListener;
    }
}