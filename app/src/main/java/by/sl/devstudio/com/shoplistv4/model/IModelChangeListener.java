package by.sl.devstudio.com.shoplistv4.model;

public interface IModelChangeListener {
    public void onChange();
}
